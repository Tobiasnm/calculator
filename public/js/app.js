var Decimal = require('decimal.js');

const screenElement = document.getElementById("screen");
const button7Element = document.getElementById("button7");
const button8Element = document.getElementById("button8");
const button9Element = document.getElementById("button9");
const buttonCElement = document.getElementById("buttonc");
const button4Element = document.getElementById("button4");
const button5Element = document.getElementById("button5");
const button6Element = document.getElementById("button6");
const buttonPlusElement = document.getElementById("button+");
const button1Element = document.getElementById("button1");
const button2Element = document.getElementById("button2");
const button3Element = document.getElementById("button3");
const buttonMinusElement = document.getElementById("button-");
const button0Element = document.getElementById("button0");
const buttonDotElement = document.getElementById("button.");
const buttonSlashElement = document.getElementById("button/");
const buttonEqualElement = document.getElementById("button=");
const buttonStarElement = document.getElementById("button*");

button7Element.addEventListener("click", clickButton, false);
button7Element.parameter = 7;
button8Element.addEventListener("click", clickButton);
button8Element.parameter = 8;
button9Element.addEventListener("click", clickButton);
button9Element.parameter = 9;
buttonCElement.addEventListener("click", clickButton);
buttonCElement.parameter = "C";
button4Element.addEventListener("click", clickButton);
button4Element.parameter = 4;
button5Element.addEventListener("click", clickButton);
button5Element.parameter = 5;
button6Element.addEventListener("click", clickButton);
button6Element.parameter = 6;
buttonPlusElement.addEventListener("click", clickButton);
buttonPlusElement.parameter = "+";
button1Element.addEventListener("click", clickButton);
button1Element.parameter = 1;
button2Element.addEventListener("click", clickButton);
button2Element.parameter = 2;
button3Element.addEventListener("click", clickButton);
button3Element.parameter = 3;
buttonMinusElement.addEventListener("click", clickButton);
buttonMinusElement.parameter = "-";
button0Element.addEventListener("click", clickButton);
button0Element.parameter = 0;
buttonDotElement.addEventListener("click", clickButton);
buttonDotElement.parameter = ".";
buttonSlashElement.addEventListener("click", clickButton);
buttonSlashElement.parameter = "/";
buttonEqualElement.addEventListener("click", clickButton);
buttonEqualElement.parameter = "=";
buttonStarElement.addEventListener("click", clickButton);
buttonStarElement.parameter = "*";

var memory;
var operator;
var newEquation = false;

function clickButton(key) {    
    if(typeof(key.currentTarget.parameter) === 'number' || key.currentTarget.parameter === "."){
        if(screenElement.innerText === 0 || newEquation === false){
            screenElement.innerText = "";
            newEquation = true;
        }
        screenElement.innerText += key.currentTarget.parameter;
    }
    else if(key.currentTarget.parameter === "C"){
        screenElement.innerText = 0;
    }
    else if(key.currentTarget.parameter === "+" || key.currentTarget.parameter === "-" || key.currentTarget.parameter === "/" || key.currentTarget.parameter === "*"){
        memory = parseFloat(screenElement.innerText,10);
        operator = key.currentTarget.parameter;
        screenElement.innerText = "";
    }
    else if(key.currentTarget.parameter === "="){
        Decimal.set({precision: 10, rounding: 2 })
        let number = parseFloat(screenElement.innerText,10);
        if(operator === "*"){
            let result = multiply(memory, number);
            result = new Decimal(result);
            screenElement.innerText = result;
        }
        if(operator === "/"){
            let result = divide(memory, number);
            result = new Decimal(result);
            screenElement.innerText = result;
        }
        if(operator === "+"){
            let result = add(memory, number);
            result = new Decimal(result);
            screenElement.innerText = result;
        }
        if(operator === "-"){
            let result = subtract(memory, number);
            result = new Decimal(result);
            screenElement.innerText = result;
              
        }
        operator = null;
        memory = null;
        newEquation = false;
    }
}

var multiply = (a,b) => Decimal.mul(a,b);
var divide = (a,b) => Decimal.div(a,b);
var add = (a,b) => Decimal.add(a,b);
var subtract = (a,b) => Decimal.sub(a,b);